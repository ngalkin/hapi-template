// @TODO: require ./config.json file


// placeholder
const config = {};

exports.register = (server, options, next) => {
  server.config = config;
  next();
};


exports.register.attributes = {
  "name": "config",
  "version": "0.0.1",
  "engines": {
    "node": ">=4.0.0"
  },
  "peerDependencies": {
    "hapi": "11.x.x"
  }
};
