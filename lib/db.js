const mongoose = require('mongoose');
const glob = require('glob');

// @TODO: via config
// connections
mongoose.cons = {
  tech:  mongoose.createConnection('mongodb://user:pass@host/techdata'),
  graph: mongoose.createConnection('mongodb://user:pass@host/graph'),
}

// models
glob.sync('../models/**.js', {cwd: __dirname}).forEach(require);


exports.register = (server, options, next) => {
  server.db = mongoose;
  next();
};

exports.register.attributes = {
  "name": "mongo",
  "version": "0.0.1",
  "engines": {
    "node": ">=4.0.0"
  },
  "peerDependencies": {
    "hapi": "11.x.x"
  }
};
