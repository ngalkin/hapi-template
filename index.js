const hapi = require('hapi');
const glue = require('glue');


glue.compose(require('./manifest'), { relativeTo: process.cwd() }, (err, server) => {
  if (err) { throw err; }

  server.start((err) => {
    if (err) { throw err; }
    console.log("The server has started on port: " + server.info.uri);
  });
});
