module.exports = {
  server: {
    app: {}
  },
  connections: [
    {
      port: 3000,
      labels: ['api']
    }
  ],
  registrations: [
    {
      plugin: {
        register: 'good',
        options: {
          opsInterval: 10000,
          reporters: [{
            reporter: 'good-console',
            events: { response: '*', ops: '*', log: '*', error: '*' }
          }]
        }
      }
    },
    {
      plugin: {
        register: './lib/config'
      }
    },
    {
      plugin: {
        register: './lib/db'
      }
    },
    {
      plugin: {
        register: 'vision'
      }
    },
    {
      plugin: {
        register: 'inert'
      }
    },
    {
      plugin: {
        register: 'lout'
      }
    },
    {
      plugin: {
        register: './modules/graph'
      }
    }
  ]
};
