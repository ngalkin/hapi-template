const joi = require('joi');

exports.register = (server, options, next) => {

  server.route({
    path: '/',
    method: 'GET',
    config: {
      validate: {
        query: { name: joi.string() }
      },
      handler: (req, res) => {
        res('Hello');
      },
    }
  });
  next();
};

exports.register.attributes = {
  name: 'graph',
  version: '1.0.0'
};
